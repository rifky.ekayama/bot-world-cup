.PHONY: all build doc

VERSION := $(shell grep "version:" package.yaml | grep -o '[0-9].[0-9].[0-9].[0-9]')
DOCROOT := $(shell stack path --local-doc-root)

all: build doc

build:
	stack build

doc:
	stack haddock
	rm -rf docs/haddock
	cp -r ${DOCROOT}/bot-world-cup-${VERSION} docs/haddock

run:
	stack exec bot-world-cup
