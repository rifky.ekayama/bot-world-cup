{-# LANGUAGE TemplateHaskell #-}

module Internal where

import           Control.Concurrent
import qualified Data.ByteString.Lazy    as BL
import           Data.Function           (on)
import           Data.Global
import           Data.IORef              (readIORef, writeIORef)
import           Data.List               as L
import           Data.Maybe              as M
import           Data.String
import           Data.Text               as T
import           Data.Text.Conversions   (convertText)
import           Data.Time.Clock
import           Protolude
import           System.Environment
import           Text.Read               (read)

-- aeson
import           Data.Aeson

-- http
import           Network.HTTP.Client     (newManager)
import           Network.HTTP.Client.TLS (tlsManagerSettings)
import           Network.HTTP.Conduit

-- telegram-api
import           Web.Telegram.API.Bot

-- internal library
import           Types

-- prepare lastEventId as global variable
declareIORef "lastEventId" [t|Int|] [|0|]

-- | Start World Cup 2018 Bot.
startBot :: IO ()
startBot = forever $ do
  -- get start time
  start <- getCurrentTime

  matchURL <- getEnv "MATCH_CURRENT_URL"
  matchResponse <- lookupTodayMatch matchURL
  let decodedMatch = eitherDecode matchResponse :: Either String [Match]
  case decodedMatch of
    Left err      -> putStrLn err
    Right matches -> do
      mapM_ processMatch $ L.filter (\x -> matchStatus x == "in progress") matches
      putStrLn $ show (L.length matches) ++ " matches processed"

  -- get end time and calculate delay
  end <- getCurrentTime
  let diff = diffUTCTime end start
      usecs = floor (toRational diff * 1000000) :: Int
      delay = 10000*1000 - usecs
  if delay > 0
    then threadDelay delay
    else return ()

-- | Lookup today's `Match`.
lookupTodayMatch :: String -> IO BL.ByteString
lookupTodayMatch url = simpleHttp url

-- | Process `Match`.
processMatch :: Match -> IO ()
processMatch m = mapM_ (processEvent m) $ sortBy (on compare eventId) $ matchHomeTeamEvents m ++ matchAwayTeamEvents m

-- | Process `Event`.
processEvent :: Match -> Event -> IO ()
processEvent match event = do
  eid <- readIORef lastEventId
  if eventId event > eid
    then do
      sendEvent match event
      updateLastEventId event
    else return ()

-- | Update `lastEventId`.
updateLastEventId :: Event -> IO ()
updateLastEventId event = writeIORef lastEventId $ eventId event

-- | Send `Event` to telegram.
sendEvent :: Match -> Event -> IO ()
sendEvent match event = do
  token <- getEnv "TELEGRAM_TOKEN"
  chatId <- getEnv "TELEGRAM_CHAT_ID"

  -- Prepare token and chatId
  let token' = Token $ convertText $ "bot" ++ token
      chatId' = ChatId (read chatId :: Int64)

  -- Prepare message request
  let newEvent = "*New Event: " <> eventTypeOfEvent event <> "*"
      player = "Player: " <> eventPlayer event <> " (" <> teamCountry (matchHomeTeam match) <> ")"
      at = "Occurs at: " <> eventTime event
      summary = teamCountry (matchHomeTeam match) <> " (" <> (show $ teamGoals (matchHomeTeam match)) <> ")"
             <> " - "
             <> teamCountry (matchAwayTeam match) <> " (" <> (show $ teamGoals (matchAwayTeam match)) <> ")"
      messages = [ newEvent
                 , player
                 , at
                 , "-"
                 , summary
                 ] :: [Text]
      message = T.unlines messages
      request = SendMessageRequest
                  chatId'
                  message
                  (Just Markdown)
                  Nothing
                  Nothing
                  Nothing
                  Nothing

  -- Send message request
  manager <- newManager tlsManagerSettings
  response <- sendMessage token' request manager
  case response of
    Left err  -> print err
    Right res -> putStrLn $ M.fromJust $ text $ result res
