# bot-world-cup

This program will continously get current world cup match and send events to Telegram every 10 seconds.

World cup API provided by [http://worldcup.sfg.io](https://github.com/estiens/world_cup_json). Endpoint that I use to get current matches:

- http://worldcup.sfg.io/matches/current

## Setup

```shell
# Setup project
stack setup

# Do an initial build of the project
stack build

# Needed for haskero (vscode plugin)
stack build intero

# Needed for Haskell GHCi Debug Adapter Phoityne (vscode plugin)
stack install phoityne-vscode

# Build the project
stack build
```

## Post Setup

### Build Executable

```shell
make build
```

Executable can be found in `.stack-work/install/x86_64-linux-tinfoX/lts-X.X/X.X.X/bin` folder.

### Build Documentation

```shell
make doc
```

Documentation can be found in `docs/haddock` folder. We can use any http server to render documentation.

```shell
# Go to docs folder
cd docs/haddock

# Run any http server, we use python3 for an example (docs can be accessed in "http://0.0.0.0:6969/")
python3 -m http.server ${2:-6969} .
```

### Usage

```shell
stack exec bot-world-cup

# or

.stack-work/install/x86_64-linux-tinfoX/lts-X.X/X.X.X/bin/bot-world-cup
```